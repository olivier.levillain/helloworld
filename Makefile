all:	Hello.class

Hello.class: Hello.java
	javac $<

check: Hello.class
	java Hello | diff - hello.out

clean:
	rm -f *.class *~
